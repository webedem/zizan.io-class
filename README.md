# README #

Zizan.io is an error reporting, monitoring analytic platform. 


### Current Status ###

* Currently supporting PHP (v. 1.05)

### How do I get set up? ###

* Before you start, you need to register for an API KEY and associate it with your domain. To do so, visit http://www.zizan.io 
* Once you register to Zizan.io, you will need to download the PHP class and import it in your current project to start error tracking.

### Dependencies ###
* PHP 5.2+
* cURL support

### Demo ###

Please find the demo in test.php file located in this repository