<?php
/**
 * Copyright 2014 Zizan.io 
 * Written by Kris Leonidou
 * Latest release: http://www.zizan.io/
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
 

class Zizanio{	
	private $configuration = array("API_KEY"=>"", "DOMAIN"=>"", "LOGFILE"=>"error.log", "TYPES"=>E_ALL, "INTERVAL"=>60, "TIMEZONE"=>"");
	private $startTime;
	private static $version = "1.05";
		
	
	// CONSTRUCTOR
	function __construct($c) {
		$mtime = microtime();
		$mtime = explode(' ', $mtime);
		$mtime = $mtime[1] + $mtime[0];
		$this->startTime = $mtime;
		$this->configuration = $c;		
		
		if(!isset($this->configuration['API_KEY']) || empty($this->configuration['API_KEY']) || !isset($this->configuration['API_KEY']) || empty($this->configuration['API_KEY'])){
			die("Zizan.io class: You need to define a valid APIKEY and DOMAIN before using the zizan.io class");			
		}
		if(!isset($this->configuration['TIMEZONE']) || empty($this->configuration['TIMEZONE'])){
			$this->configuration['TIMEZONE'] = date_default_timezone_get();
		}
		
		date_default_timezone_set($this->configuration['TIMEZONE']);
					
		error_reporting($this->configuration['TYPES']);
		ini_set("log_errors", 1);
		ini_set("error_log", $this->configuration['LOGFILE']);		
				
		set_exception_handler(array($this, 'exception_handler'));
		set_error_handler(array($this, 'error_handler'));
		register_shutdown_function(array($this, 'fatal_handler'));
	
		if(!file_exists($this->configuration['LOGFILE'])){
			$fp = fopen($this->configuration['LOGFILE'], "w");
			fclose($fp);	
		}
		else{
			if(filesize($this->configuration['LOGFILE'])>0){				
				$this->Flush();	
			}
		}
		if(!file_exists(".zizanio.log")){
			$fp = fopen(".zizanio.log", "w");
			fclose($fp);	
		}
	}
	
	public function fatal_handler() {	 
		$err = error_get_last();
		$errmsg = $err['type']." ".$err['message']." in ".$err['file']." on line ".$err['line'];
		$debugParams = debug_backtrace();
		$fields = array('apikey' => $this->configuration['API_KEY'], 'domain'=>$this->configuration['DOMAIN'],'log' =>str_replace("\\", "|", $errmsg), 'mem'=>memory_get_peak_usage(), 'ip'=>$_SERVER['REMOTE_ADDR'], 'agent'=>$_SERVER['HTTP_USER_AGENT'], 'timestamp'=>time(), 'debug'=>str_replace("\\", "|", (!isset($debugParams[0]['file']) ? '' : $debugParams[0]['file'])).' / line:'.(!isset($debugParams[0]['line']) ? '' : $debugParams[0]['line']));
		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');				

		$ch = curl_init();		
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_URL, "http://www.zizan.io/post/fetch");
		curl_setopt($ch,CURLOPT_POST, 2);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		
		if($result=='success'){
			if(file_exists($this->configuration['LOGFILE'])) { unlink($this->configuration['LOGFILE']); }
		}				
		curl_close($ch);		
	}
	
	
	public function exception_handler($exception) {	
	    $fields = array('apikey' => $this->configuration['API_KEY'], 'domain'=>$this->configuration['DOMAIN'],'log' =>"[".date("d-M-Y H:i:s")."] Exception: ".$exception->getMessage(), 'mem'=>memory_get_peak_usage(), 'ip'=>$_SERVER['REMOTE_ADDR'], 'agent'=>$_SERVER['HTTP_USER_AGENT'], 'timestamp'=>time(), 'debug'=>str_replace("\\", "|", $exception->getFile()).' / line:'.$exception->getLine());
		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');				

		$ch = curl_init();		
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_URL, "http://www.zizan.io/post/fetch");
		curl_setopt($ch,CURLOPT_POST, 2);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		
		if($result=='success'){
			if(file_exists($this->configuration['LOGFILE'])) { unlink($this->configuration['LOGFILE']); }
		}				
		curl_close($ch);
	}
	
	public function error_handler($errno, $errstr, $errfile, $errline) {
		
	    $fields = array('apikey' => $this->configuration['API_KEY'], 'domain'=>$this->configuration['DOMAIN'],'log' =>"[".date("d-M-Y H:i:s")."] Exception: ".$errstr, 'mem'=>memory_get_peak_usage(), 'ip'=>$_SERVER['REMOTE_ADDR'], 'agent'=>$_SERVER['HTTP_USER_AGENT'], 'timestamp'=>time(), 'debug'=>str_replace("\\", "|", $errfile).' / line:'.$errline);
		$fields_string = "";
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }
		rtrim($fields_string, '&');				

		$ch = curl_init();		
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch,CURLOPT_URL, "http://www.zizan.io/post/fetch");
		curl_setopt($ch,CURLOPT_POST, 2);
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		$result = curl_exec($ch);
		
		if($result=='success'){
			if(file_exists($this->configuration['LOGFILE'])) { unlink($this->configuration['LOGFILE']); }
		}				
		curl_close($ch);
	}
	
	public function Flush(){	
		if(file_exists($this->configuration['LOGFILE']) && filesize($this->configuration['LOGFILE'])>0){
			$debugParams = debug_backtrace();	
			$fields = array('apikey' => $this->configuration['API_KEY'], 'domain'=>$this->configuration['DOMAIN'],'log' =>str_replace("\\", "|", file_get_contents($this->configuration['LOGFILE'])), 'mem'=>memory_get_peak_usage(), 'ip'=>$_SERVER['REMOTE_ADDR'], 'agent'=>$_SERVER['HTTP_USER_AGENT'], 'timestamp'=>time(), 'debug'=>str_replace("\\", "|", $debugParams[0]['file']).' / line:'.$debugParams[0]['line']);
			$fields_string = "";
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }
			rtrim($fields_string, '&');				
	
			$ch = curl_init();		
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,CURLOPT_URL, "http://www.zizan.io/post/fetch");
			curl_setopt($ch,CURLOPT_POST, 2);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			$result = curl_exec($ch);
			
			if($result=='success'){
				if(file_exists($this->configuration['LOGFILE'])) { unlink($this->configuration['LOGFILE']); }
			}				
			curl_close($ch);	
		}	
		$this->MemLog();						
	}
	
	private function MemLog(){
		if(file_exists(".zizanio.log")){				
			if(time() > (filemtime(".zizanio.log") + intval($this->configuration['INTERVAL']))){
				$mtime = microtime();
				$mtime = explode(' ', $mtime);
				$mtime = $mtime[1] + $mtime[0];
				$finish = $mtime;
				$this->loadTime = round(($finish - $this->startTime), 4);
				$get_vars = "GET: ";
				foreach($_GET as $key=>$item){
					$get_vars .= "#".$key."=".$item.", ";
				}
				if(count($_GET)>0){
					$get_vars = substr($get_vars, 0, -2);
				}
				else{
					$get_vars = "";
				}
				
				$post_vars = "POST: ";
				foreach($_POST as $key=>$item){
					$post_vars .= "@".$key."=".$item.", ";
				}
				if(count($_POST)>0){
					$post_vars = substr($post_vars, 0, -2);
				}
				else{
					$post_vars = "";	
				}
				
				$fields = array('apikey' => $this->configuration['API_KEY'], 'domain'=>$this->configuration['DOMAIN'],'script'=>$_SERVER['SCRIPT_NAME'], 'mem'=>memory_get_peak_usage(), 'ip'=>$_SERVER['REMOTE_ADDR'], 'parameters'=>$get_vars.$post_vars, 'timestamp'=>time(), 'loadtime'=>floatval($this->loadTime));
				$fields_string = "";
				foreach($fields as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }
				rtrim($fields_string, '&');				
		
				$ch = curl_init();		
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch,CURLOPT_URL, "http://www.zizan.io/post/mem");
				curl_setopt($ch,CURLOPT_POST, 2);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
				$result = curl_exec($ch);
				
				if($result=='success'){
					if(file_exists(".zizanio.log")) { unlink(".zizanio.log"); }
				}				
				curl_close($ch);	
			}
		}
	}
	
	public function Log($message){			
			$debugParams = debug_backtrace();
			
			$fields = array('apikey' => $this->configuration['API_KEY'], 'domain'=>$this->configuration['DOMAIN'],'log'=>"[".date("Y-m-d H:i:s")."] ".str_replace("-", "_", str_replace("\\", "|",htmlentities(preg_replace("/(#\w+)/", "$1", $message)))), 'mem'=>memory_get_peak_usage(), 'ip'=>$_SERVER['REMOTE_ADDR'], 'agent'=>$_SERVER['HTTP_USER_AGENT'],'debug'=>str_replace("\\", "|", $debugParams[0]['file']).' / line:'.$debugParams[0]['line']);
			$fields_string = "";
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.urlencode($value).'&'; }
			rtrim($fields_string, '&');				
	
			$ch = curl_init();		
			curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch,CURLOPT_URL, "http://www.zizan.io/post/fetch");
			curl_setopt($ch,CURLOPT_POST, 2);
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
			$result = curl_exec($ch);							
			curl_close($ch);
		}
	
	
}

	
?>
