<?php
	// Import the Zizan.io class
	require_once("zizanio.class.php");	

	// Let's instantiate the class
	$zizanio = new Zizanio(
		  array('API_KEY'=>"", /* YOUR API KEY */
				'DOMAIN'=>"", /* YOUR DOMAIN */
				'LOGFILE'=>"error.log", /* THE NAME OF YOUR PHP LOG FILE */
				'TYPES'=>E_ALL, 	/* PHP ERROR TYPES TO CAPTURE: DEFAULT=E_ALL */
				'INTERVAL'=>10, /* MEMORY LOGGING INTERVAL IN SECONDS */
				'TIMEZONE'=>'Europe/Athens')); /* INDICATE A PHP VALID TIMEZONE IF YOU DON'T WANT TO USE THE SERVER'S TIMEZONE */	
	

	
	/* ======= LOGS =========*/
	// Log any event you want
	// Note that file, code line and timestamp of event are automatically gathered by Zizan.io
	// Note that if you log the exact same message more than once, zizanio will only capture the first run.
	// To make each log unique you need to have a different message. For example, you can add time() at the 
	// end of your log message if you really need to log the exact same event 
	
	/* Simple log: */
	$zizanio->Log("Something happened here and I want to log it.");
	
	/* Log with hashtags so you can filter by hashtag later on the Zizan.io dashboard */
	// Only letters, numbers and underscore are accepted in hashtags
	$zizanio->Log("User registration success. #user registration #developer");
	

	/* ======= EXCEPTIONS =========*/
	// Exceptions and PHP errors are catpured automatically by Zizan.io
	// Note that file, code line and timestamp of event are automatically gathered by Zizan.io
	
	// Uncomment the nexto see how it works
	//throw new Exception("Something happened here that wasn't supposed to");	
	
	/* ======= ERRORS  =========*/
	// Exceptions and PHP errors are catpured automatically by Zizan.io
	// Note that file, code line and timestamp of event are automatically gathered by Zizan.io
	
	// Uncomment the next line to see how it works
	//require('file_that_does_not_exists.php');	
	

	/* PUT THIS LINE OF CODE AT THE VERY END */
	// the following line will flush all PHP errors to zizanio platform. 
	// Note that any errors that cause the script to terminate will be 
	// automatically flushed to zizanio platform immediatelly after they occur
	
	$zizanio->Flush();  
?>